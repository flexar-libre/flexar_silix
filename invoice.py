# This file is part of the flexar_silix module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from trytond.transaction import Transaction


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'

    def get_move(self):
        move = super().get_move()
        if self.description:
            move.description = self.description
        return move

    def set_pyafipws_concept(self):
        '''
        set pyafipws_concept researching the product lines.
        '''
        products = {'1': 0, '2': 0}
        self.pyafipws_concept = ''
        for line in self.lines:
            if line.type != 'line':
                continue
            if line.product:
                if line.product.type == 'goods':
                    products['1'] += 1
                elif line.product.type == 'service':
                    products['2'] += 1
                elif line.product.type == 'kit':
                    for component in line.product.components_used:
                        if component.product.type == 'goods':
                            products['1'] += 1
                        elif line.product.type == 'service':
                            component['2'] += 1
            else:
                products['2'] += 1

        if products['1'] != 0 and products['2'] != 0:
            self.pyafipws_concept = '3'
        elif products['1'] != 0:
            self.pyafipws_concept = '1'
        elif products['2'] != 0:
            self.pyafipws_concept = '2'


class InvoiceLine(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    product_customer = fields.Function(fields.Many2One(
        'sale.product_customer', "Customer's Product"),
        'get_product_customer')
    account_domain = fields.Function(fields.Many2Many('account.account',
        None, None, 'Account domain'), 'on_change_with_account_domain')

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.account.domain = [('id', 'in', Eval('account_domain', []))]

    def get_product_customer(self, name):
        pool = Pool()
        SaleLine = pool.get('sale.line')
        if isinstance(self.origin, SaleLine):
            if self.origin.product_customer:
                return self.origin.product_customer.id
        return None

    @fields.depends('company', 'invoice', '_parent_invoice.type',
        'invoice_type', '_parent_invoice.pos')
    def on_change_with_account_domain(self, name=None):
        cursor = Transaction().connection.cursor()
        pool = Pool()
        Account = pool.get('account.account')
        AccountType = pool.get('account.account.type')

        sql_where = ''

        invoice_type = self.invoice and self.invoice.type or self.invoice_type
        if invoice_type == 'out':
            if (self.invoice and self.invoice.pos and
                    self.invoice.pos.pos_type == 'manual'):
                pass
            else:
                sql_where = 'AND t.revenue IS TRUE '
        elif invoice_type == 'in':
            sql_where = ('AND (t.expense IS TRUE OR t.debt IS TRUE '
                'OR t.assets IS TRUE ) ')
        else:
            sql_where = ('AND (t.revenue IS TRUE OR t.expense IS TRUE '
                'OR t.debt IS TRUE OR t.assets IS TRUE ) ')

        cursor.execute('SELECT a.id '
            'FROM "' + Account._table + '" a '
                'INNER JOIN "' + AccountType._table + '" t '
                'ON t.id = a.type '
            'WHERE a.closed IS FALSE '
                'AND a.company = %s '
                + sql_where,
            (self.company.id,))
        return [x[0] for x in cursor.fetchall()]

    @fields.depends('invoice', 'invoice_type', 'party',
        '_parent_invoice.type', '_parent_invoice.party')
    def on_change_product(self):
        super().on_change_product()
        party = None
        if self.invoice and self.invoice.party:
            party = self.invoice.party
        elif self.party:
            party = self.party
        if not party:
            return
        if self.invoice and self.invoice.type:
            type_ = self.invoice.type
        else:
            type_ = self.invoice_type
        if not type_:
            return

        if type_ == 'in':
            account = party.account_expense_used
        else:
            account = party.account_revenue_used
        if account:
            self.account = account
