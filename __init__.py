# This file is part of the flexar_silix module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

from trytond.pool import Pool
from . import company
from . import sale
from . import purchase
from . import shipment
from . import stock
from . import invoice
from . import account
from . import account_check_ar
from . import account_voucher_ar
from . import party
from . import product


def register():
    Pool.register(
        company.Company,
        sale.Sale,
        sale.SaleLine,
        purchase.Purchase,
        purchase.PurchaseLine,
        shipment.ShipmentOut,
        stock.Move,
        invoice.Invoice,
        invoice.InvoiceLine,
        account.Configuration,
        account.ConfigurationDefaultVoucher,
        account.MoveLine,
        account.GeneralLedgerLine,
        account_check_ar.ImportThirdCheckStart,
        account_voucher_ar.AccountVoucher,
        account_voucher_ar.AccountVoucherLine,
        party.Party,
        party.PartyAccount,
        product.Category,
        product.CategoryAccount,
        module='flexar_silix', type_='model')
    Pool.register(
        account.OpenStatementOfAccountDollar,
        account_check_ar.ImportThirdCheck,
        module='flexar_silix', type_='wizard')
    Pool.register(
        shipment.DeliveryNote,
        account.StatementOfAccountDollarReport,
        account.StatementOfAccountDollarSpreadsheet,
        module='flexar_silix', type_='report')
