# This file is part of the flexar_silix module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from decimal import Decimal

from sql import Literal, Null
from sql.aggregate import Sum
from sql.conditionals import Coalesce

from trytond.model import fields
from trytond.modules.currency.fields import Monetary
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval
from trytond.tools import grouped_slice, reduce_ids
from trytond.transaction import Transaction


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'

    account_expense = fields.MultiValue(fields.Many2One(
        'account.account', 'Account Expense',
        domain=[
            ('closed', '!=', True),
            ('type.expense', '=', True),
            ('company', '=', Eval('context', {}).get('company', -1)),
            ],
        states={
            'invisible': ~Eval('context', {}).get('company'),
            },
        help='For imports'))
    account_revenue = fields.MultiValue(fields.Many2One(
        'account.account', 'Account Revenue',
        domain=[
            ('closed', '!=', True),
            ('type.revenue', '=', True),
            ('company', '=', Eval('context', {}).get('company', -1)),
            ],
        states={
            'invisible': ~Eval('context', {}).get('company'),
            },
        help='For exports'))
    secondary_currency = fields.Function(fields.Many2One(
            'currency.currency', "Secondary Currency"),
        'get_secondary_currency')
    receivable_secondary_today = fields.Function(Monetary(
            "Receivable Secondary Today", currency='secondary_currency',
            digits='secondary_currency'),
        'get_receivable_payable_secondary',
        searcher='search_receivable_payable_secondary')
    payable_secondary_today = fields.Function(Monetary(
            "Payable Secondary Today", currency='secondary_currency',
            digits='secondary_currency'),
        'get_receivable_payable_secondary',
        searcher='search_receivable_payable_secondary')

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field in ['account_expense', 'account_revenue']:
            return pool.get('party.party.account')
        return super(Party, cls).multivalue_model(field)

    @property
    def account_expense_used(self):
        account = self.account_expense
        if account:
            return account.current()

    @property
    def account_revenue_used(self):
        account = self.account_revenue
        if account:
            return account.current()

    def get_secondary_currency(self, name):
        Currency = Pool().get('currency.currency')

        id_dollar, = Currency.search([('code', '=', 'USD')])
        return id_dollar and id_dollar.id or None

    @classmethod
    def get_receivable_payable_secondary(cls, parties, names):
        '''
        Function to compute receivable, payable (today or not) for party ids.
        '''
        result = {}
        pool = Pool()
        MoveLine = pool.get('account.move.line')
        Account = pool.get('account.account')
        AccountType = pool.get('account.account.type')
        Currency = pool.get('currency.currency')
        User = pool.get('res.user')
        Date = pool.get('ir.date')
        cursor = Transaction().connection.cursor()

        line = MoveLine.__table__()
        account = Account.__table__()
        account_type = AccountType.__table__()

        for name in names:
            if name not in ('receivable_secondary', 'payable_secondary',
                    'receivable_secondary_today', 'payable_secondary_today'):
                raise Exception('Bad argument')
            result[name] = dict((p.id, Decimal('0.0')) for p in parties)

        user = User(Transaction().user)
        if not user.company:
            return result
        company_id = user.company.id

        id_dollar = Currency.search([('code', '=', 'USD')])
        if not id_dollar:
            return result
        secondary_currency = Currency.browse([id_dollar[0].id])

        exp = Decimal(str(10.0 ** -secondary_currency[0].digits))
        with Transaction().set_context(company=company_id):
            today = Date.today()

        amount = Sum(Coalesce(line.amount_second_currency, 0))
        for name in names:
            code = name
            today_where = Literal(True)
            if name in (
                    'receivable_secondary_today', 'payable_secondary_today'):
                code = name[:-16]
                today_where = ((line.maturity_date <= today)
                    | (line.maturity_date == Null))
            for sub_parties in grouped_slice(parties):
                sub_ids = [p.id for p in sub_parties]
                party_where = reduce_ids(line.party, sub_ids)
                cursor.execute(*line.join(account,
                        condition=account.id == line.account
                        ).join(account_type,
                        condition=account.type == account_type.id
                        ).select(line.party, amount,
                        where=(getattr(account_type, code)
                            & (line.reconciliation == Null)
                            & (account.company == company_id)
                            & party_where
                            & today_where),
                        group_by=line.party))
                for party, value in cursor:
                    # SQLite uses float for SUM
                    if not isinstance(value, Decimal):
                        value = Decimal(str(value))
                    result[name][party] = value.quantize(exp)
        return result

    @classmethod
    def search_receivable_payable_secondary(cls, name, clause):
        pool = Pool()
        MoveLine = pool.get('account.move.line')
        Account = pool.get('account.account')
        AccountType = pool.get('account.account.type')
        User = pool.get('res.user')
        Date = pool.get('ir.date')

        line = MoveLine.__table__()
        account = Account.__table__()
        account_type = AccountType.__table__()

        if name not in ('receivable_secondary', 'payable_secondary',
                'receivable_secondary_today', 'payable_secondary_today'):
            raise Exception('Bad argument')
        _, operator, value = clause

        user = User(Transaction().user)
        if not user.company:
            return []
        company_id = user.company.id
        with Transaction().set_context(company=company_id):
            today = Date.today()

        code = name
        today_query = Literal(True)
        if name in ('receivable_secondary_today', 'payable_secondary_today'):
            code = name[:-16]
            today_query = ((line.maturity_date <= today)
                | (line.maturity_date == Null))

        Operator = fields.SQL_OPERATORS[operator]

        # Need to cast numeric for sqlite
        cast_ = MoveLine.debit.sql_cast
        amount = cast_(Sum(Coalesce(line.amount_second_currency, 0)))
        if operator in {'in', 'not in'}:
            value = [cast_(Literal(Decimal(v or 0))) for v in value]
        else:
            value = cast_(Literal(Decimal(value or 0)))
        query = (line.join(account, condition=account.id == line.account
                ).join(account_type, condition=account.type == account_type.id
                ).select(line.party,
                where=(getattr(account_type, code)
                    & (line.party != Null)
                    & (line.reconciliation == Null)
                    & (account.company == company_id)
                    & today_query),
                group_by=line.party,
                having=Operator(amount, value)))
        return [('id', 'in', query)]

    @classmethod
    def copy(cls, parties, default=None):
        context = Transaction().context
        default = default.copy() if default else {}
        if context.get('_check_access'):
            fields = ['account_expense', 'account_revenue']
            default_values = cls.default_get(fields, with_rec_name=False)
            for fname in fields:
                default.setdefault(fname, default_values.get(fname))
        return super().copy(parties, default=default)

    def set_padron(self, padron, button_afip=True):
        prev_iva_condition = self.iva_condition
        super().set_padron(padron, button_afip)
        if (self.iva_condition == 'consumidor_final' and
                prev_iva_condition == 'responsable_inscripto'):
            self.iva_condition = 'responsable_inscripto'
            self.save()


class PartyAccount(metaclass=PoolMeta):
    __name__ = 'party.party.account'

    account_expense = fields.Many2One(
        'account.account', 'Account Expense',
        domain=[
            ('type.expense', '=', True),
            ('company', '=', Eval('company', -1)),
            ],
        ondelete='RESTRICT')
    account_revenue = fields.Many2One(
        'account.account', 'Account Revenue',
        domain=[
            ('type.revenue', '=', True),
            ('company', '=', Eval('company', -1)),
            ],
        ondelete='RESTRICT')
