# This file is part of the flexar_silix module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

from trytond.model import fields
from trytond.pool import PoolMeta, Pool


class Move(metaclass=PoolMeta):
    __name__ = 'stock.move'

    product_customer = fields.Function(fields.Many2One(
        'sale.product_customer', "Customer's Product"),
        'get_product_customer')

    def get_product_customer(self, name):
        pool = Pool()
        SaleLine = pool.get('sale.line')
        StockMove = pool.get('stock.move')
        if isinstance(self.origin, SaleLine):
            if self.origin.product_customer:
                return self.origin.product_customer.id
        elif isinstance(self.origin, StockMove):
            if isinstance(self.origin.origin, SaleLine):
                if self.origin.origin.product_customer:
                    return self.origin.origin.product_customer.id
        return None
