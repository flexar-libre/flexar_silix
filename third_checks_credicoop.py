# This file is part of the flexar_silix module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from datetime import datetime
from decimal import Decimal
import csv
import tempfile


def _date(value):
    v = str(value).strip()
    return datetime.strptime(v, '%d/%m/%Y').date()


def _string(value):
    return value.strip()


def _check_number(value):
    return value.strip().zfill(8)


def _bank_code(value):
    return value.strip().zfill(3)


def _amount(value):
    if not value:
        return Decimal('0.0')
    return Decimal(str(value).replace('.', '').replace(',', '.'))


CHECK = {
    'number': (0, _check_number),
    'debit_date': (2, _date),
    'amount': (4, _amount),
    'party': (5, _string),
    'cuit': (6, _string),
    'reason': (7, _string),
    'check_id': (8, _string),
    'endorsed': (9, _string),
    'bank_code': (12, _bank_code),
    'bank': (13, _string),
    }


class Credicoop(object):

    def __init__(self, name, encoding='windows-1252'):
        self.checks = []
        self._parse(name)

    def _parse(self, infile):
        # Convert byte string (b' ') to string and replace carriage returns
        filedata = str(infile)[2:-1]
        filedata = filedata.replace('\\n', '\n')
        with tempfile.NamedTemporaryFile(mode='w') as temp:
            # Write to temp file and reset it to beginning,
            # then pass to csv reader
            temp.write(filedata)
            temp.seek(0)
            csv_reader = csv.reader(
                open(temp.name), delimiter=',', quotechar='"')
            next(csv_reader)  # skip header
            for row in csv_reader:
                check = Check()
                self._parse_check(row, check, CHECK)
                self.checks.append(check)

    def _parse_check(self, row, check, desc):
        for name, (col, parser) in desc.items():
            value = parser(row[col])
            setattr(check, name, value)


class Check(object):
    __slots__ = list(CHECK.keys())

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
