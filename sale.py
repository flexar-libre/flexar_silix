# This file is part of the flexar_silix module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

from trytond.pool import PoolMeta


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    def _get_invoice_sale(self):
        invoice = super()._get_invoice_sale()
        invoice.comment = self.description
        return invoice

    def _get_shipment_sale(self, Shipment, key):
        shipment = super()._get_shipment_sale(Shipment, key)
        shipment.reference = self.reference
        if Shipment.__name__ == 'stock.shipment.out':
            shipment.comment = self.comment
        return shipment


class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'

    def get_invoice_line(self):
        invoice_lines = super().get_invoice_line()
        account = (self.sale.party and
            self.sale.party.account_revenue_used)
        if account:
            for invoice_line in invoice_lines:
                if invoice_line.type == 'line':
                    invoice_line.account = account
        return invoice_lines
