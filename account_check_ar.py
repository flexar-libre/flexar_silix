# This file is part of the flexar_silix module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.model import fields, ModelView
from trytond.pool import Pool
from trytond.wizard import Button, StateAction, StateView, Wizard
from .third_checks_credicoop import Credicoop
from .exceptions import ImportThirdCheckError


class ImportThirdCheckStart(ModelView):
    "Third Check Import Start"
    __name__ = 'account.third.check.import.start'

    bank_format = fields.Selection(
        [('credicoop', 'Banco Credicoop')], "Bank Format",
        required=True, translate=False)
    file_ = fields.Binary('File', required=True)


class ImportThirdCheck(Wizard):
    "Third Checks Import"
    __name__ = 'account.third.check.import'
    start = StateView('account.third.check.import.start',
        'flexar_silix.third_check_import_start_view_form', [
            Button("Cancel", 'end', 'tryton-cancel'),
            Button("Import", 'import_', 'tryton-ok', default=True),
            ])
    import_ = StateAction('account_check_ar.act_third_check_tree')

    def do_import_(self, action):
        pool = Pool()
        ThirdCheck = pool.get('account.third.check')
        checks = list(getattr(self, 'parse_%s' % self.start.bank_format)())
        ThirdCheck.save(checks)
        self.start.file_ = None

        data = {'res_id': list(map(int, checks))}
        if len(checks) == 1:
            action['views'].reverse()
        return action, data

    def parse_credicoop(self, encoding='windows-1252'):
        file_ = self.start.file_
        credicoop = Credicoop(file_)
        for bc_check in credicoop.checks:
            yield self.credicoop_check(bc_check)

    def credicoop_check(self, bc_check):
        pool = Pool()
        ThirdCheck = pool.get('account.third.check')
        Bank = pool.get('bank')
        Currency = pool.get('currency.currency')

        check = ThirdCheck()
        banks = Bank.search([
                ('bcra_code', '=', bc_check.bank_code),
                ])
        check.bank = banks and banks[0] or None
        if not check.bank:
            raise ImportThirdCheckError(
                'The bank "%(bank)s" from check "%(number)s" was '
                'not found. Please create the missing bank.' % {
                    'bank': bc_check.bank, 'number': bc_check.number})
        currencies = Currency.search([('code', '=', 'ARS')])
        check.currency = currencies and currencies[0] or None
        check.amount = bc_check.amount
        check.name = bc_check.number
        check.date_in = bc_check.debit_date
        check.date = bc_check.debit_date
        check.state = 'draft'
        check.electronic = True
        check.signatory = bc_check.party
        check.endorsed = bc_check.endorsed
        return check
