# This file is part of the flexar_silix module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

from trytond.pool import PoolMeta


class Purchase(metaclass=PoolMeta):
    __name__ = 'purchase.purchase'

    def _get_invoice_purchase(self):
        invoice = super()._get_invoice_purchase()
        invoice.comment = self.description
        return invoice


class PurchaseLine(metaclass=PoolMeta):
    __name__ = 'purchase.line'

    def get_invoice_line(self):
        invoice_lines = super().get_invoice_line()
        account = (self.purchase.party and
            self.purchase.party.account_expense_used)
        if account:
            for invoice_line in invoice_lines:
                if invoice_line.type == 'line':
                    invoice_line.account = account
        return invoice_lines
