# This file is part of the flexar_silix module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

from trytond.pool import PoolMeta
from trytond.pyson import Eval


class Category(metaclass=PoolMeta):
    __name__ = 'product.category'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        new_domain = [
            ('closed', '!=', True),
            ('type', '!=', None),
            ('company', '=', Eval('context', {}).get('company', -1)),
            ]
        cls.account_expense.domain = new_domain


class CategoryAccount(metaclass=PoolMeta):
    __name__ = 'product.category.account'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        new_domain = [
            ('closed', '!=', True),
            ('type', '!=', None),
            ('company', '=', Eval('company', -1)),
            ]
        cls.account_expense.domain = new_domain
