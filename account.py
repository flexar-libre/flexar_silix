# This file is part of the flexar_silix module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from decimal import Decimal

from trytond.model import fields, ModelSQL, ValueMixin
from trytond.modules.currency.fields import Monetary
from trytond.pool import Pool, PoolMeta
from trytond.pyson import PYSONEncoder
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateAction
from trytond.modules.company import CompanyReport


class Configuration(metaclass=PoolMeta):
    __name__ = 'account.configuration'

    default_voucher_writeoff_dollar = fields.MultiValue(fields.Many2One(
        'account.move.reconcile.write_off',
        'Default Write Off Method (Dollar)'))

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field in {'default_voucher_writeoff_dollar'}:
            return pool.get('account.configuration.default_voucher')

        return super().multivalue_model(field)


class ConfigurationDefaultVoucher(ModelSQL, ValueMixin):
    __name__ = 'account.configuration.default_voucher'

    default_voucher_writeoff_dollar = fields.Many2One(
        'account.move.reconcile.write_off',
        'Default Write Off Method (Dollar)')


class MoveLine(metaclass=PoolMeta):
    __name__ = 'account.move.line'

    debit_second_currency = fields.Function(Monetary('Debit',
        currency='second_currency', digits='second_currency'),
        'get_amount_second_currency')
    credit_second_currency = fields.Function(Monetary('Credit',
        currency='second_currency', digits='second_currency'),
        'get_amount_second_currency')
    balance_second_currency = fields.Function(fields.Numeric('Balance',
        digits=(16, 2)), 'get_balance_second_currency')

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.party.states['invisible'] = False

    @classmethod
    def get_amount_second_currency(cls, lines, name):
        second_currency_lines = [line for line in lines
            if line.second_currency]
        res = {}
        for line in second_currency_lines:
            if name == 'debit_second_currency':
                res[line.id] = (line.debit > Decimal('0.0') and
                    line.amount_second_currency or Decimal('0.0'))
            elif name == 'credit_second_currency':
                res[line.id] = (line.credit > Decimal('0.0') and
                    line.amount_second_currency * -1 or Decimal('0.0'))
        return res

    @classmethod
    def get_balance_second_currency(cls, lines, name):
        if not lines:
            return {}

        ids = [x.id for x in lines]
        res = {}.fromkeys(ids, Decimal('0.0'))
        fiscalyear_id = journal_id = period_id = account_id = None
        from_date = to_date = None
        company_id = party_id = account_kind = None

        from_fiscalyear = where_fiscalyear = ''
        if Transaction().context.get('fiscalyear'):
            fiscalyear_id = int(Transaction().context.get('fiscalyear'))
        if fiscalyear_id:
            from_fiscalyear = '''
                LEFT JOIN account_period ap ON (ap.id=am.period)
                LEFT JOIN account_fiscalyear af ON (af.id=ap.fiscalyear)
                '''
            where_fiscalyear = '''
                ap.fiscalyear = %d AND
                ''' % fiscalyear_id

        where_journal = ''
        if Transaction().context.get('journal'):
            journal_id = int(Transaction().context.get('journal'))
        if journal_id:
            where_journal = '''
                am.journal = %d AND
                ''' % journal_id

        where_period = ''
        if Transaction().context.get('period'):
            period_id = int(Transaction().context.get('period'))
        if period_id:
            where_period = '''
                am.period = %d AND
                ''' % period_id

        where_from_date = ''
        if Transaction().context.get('from_date'):
            from_date = Transaction().context.get('from_date')
        if from_date:
            where_from_date = '''
                am.date >= '%s' AND
                ''' % from_date

        where_to_date = ''
        if Transaction().context.get('to_date'):
            to_date = Transaction().context.get('to_date')
        if to_date:
            where_to_date = '''
                am.date <= '%s' AND
                ''' % to_date

        where_account = ''
        if Transaction().context.get('account'):
            account_id = int(Transaction().context.get('account'))
        if account_id:
            where_account = '''
                aml.account = %d AND
                ''' % account_id

        where_company = ''
        if Transaction().context.get('company'):
            company_id = int(Transaction().context.get('company'))
        if company_id:
            where_company = '''
                am.company = %d AND
                ''' % company_id

        where_party = ''
        if Transaction().context.get('party'):
            party_id = int(Transaction().context.get('party'))
        if party_id:
            where_party = '''
                aml.party = %d AND
                ''' % party_id

        from_account_kind = where_account_kind = ''
        if Transaction().context.get('account_kind'):
            account_kind = list(Transaction().context.get('account_kind'))
        if account_kind:
            from_account_kind = ', account_account a, account_account_type at'
            where_account_kind = ('aml.account = a.id '
                'AND a.type = at.id '
                'AND (at.payable IS TRUE OR at.receivable IS TRUE) AND ')

        cursor = Transaction().connection.cursor()
        for line in lines:
            id = line.id
            date = line.move.date
            number = line.move.number
            amount = line.amount_second_currency or Decimal('0')

            cursor.execute("""
                SELECT
                    SUM(amount_second_currency)
                FROM
                    account_move am""" + from_fiscalyear + """,
                    account_move_line aml""" + from_account_kind + """
                WHERE """ + where_fiscalyear + where_journal
                    + where_from_date + where_to_date + where_company
                    + where_period + where_account + where_party
                    + where_account_kind + """
                    aml.move = am.id
                    AND (
                        am.date < %s
                        OR (am.date = %s AND am.number < %s)
                        OR (am.date = %s AND am.number = %s
                            AND aml.id < %s)
                    )
                """, (date, date, number, date, number, id))
            balance = cursor.fetchone()[0] or Decimal('0.0')
            if not isinstance(balance, Decimal):
                balance = Decimal(balance)
            res[id] = balance + amount
        return res


class GeneralLedgerLine(metaclass=PoolMeta):
    __name__ = 'account.general_ledger.line'

    statement_origin = fields.Function(fields.Reference(
        "Statement Origin", selection='get_statement_origin',
        states={'readonly': True}), 'get_related_statement')

    @classmethod
    def get_statement_origin(cls):
        Model = Pool().get('ir.model')
        get_name = Model.get_name
        models = ['account.statement', 'account.statement.line']
        return [(None, '')] + [(m, get_name(m)) for m in models]

    def get_related_statement(self, name):
        pool = Pool()
        StatementLine = pool.get('account.statement.line')
        MoveLine = pool.get('account.move.line')
        LinePaymode = pool.get('account.voucher.line.paymode')
        ThirdCheck = pool.get('account.third.check')

        if self.move:
            if self.move.origin:
                origin = str(self.move.origin)
                if origin.split(',')[0] in [
                        'account.statement', 'account.statement.line']:
                    return origin
                elif origin.split(',')[0] in ['account.voucher']:
                    # Los Cheques emitidos se detectan en el voucher
                    paymodes = LinePaymode.search([
                        ('voucher', '=', int(origin.split(',')[1])),
                        ('related_statement_line', '!=', None)
                        ])
                    for p in paymodes:
                        if (p.pay_amount == self.credit or
                                p.pay_amount == self.debit):
                            return str(p.related_statement_line)
            else:
                lines = MoveLine.search([
                    ('move', '=', self.move.id),
                    ('account', '=', self.account.id),
                    ('related_statement_line', '!=', None),
                    ('credit', '=', self.credit),
                    ('debit', '=', self.debit)
                    ])
                for l in lines:
                    sl = StatementLine.search([
                        ('related_to', '=', 'account.move.line,' + str(l.id))
                        ])
                    if sl:
                        return str(l.related_statement_line)

                # Cheques de terceros depositados:
                # se revisa 'description' del asiento para intentar
                # detectar el numero del cheque
                amount = self.credit > 0 and self.credit or self.debit
                move_description = self.move.description
                for data in move_description.split():
                    third_checks = ThirdCheck.search([
                        ('name', '=', data),
                        ('amount', '=', amount),
                        ('currency', '=', self.currency),
                        ('related_statement_line', '!=', None)
                        ])
                    for check in third_checks:
                        related = 'account.third.check,' + str(check.id)
                        sl = StatementLine.search([
                            ('related_to', '=', related)
                            ])
                        if sl:
                            return str(check.related_statement_line)
        return None


class OpenStatementOfAccountDollar(Wizard):
    'Open Statement of Account Dollar'
    __name__ = 'account.move.line.balance.dollar'

    start_state = 'open_'
    open_ = StateAction('flexar_silix.act_statement_of_account_dollar')

    def do_open_(self, action):
        pool = Pool()
        Currency = pool.get('currency.currency')
        Party = pool.get('party.party')

        id_dollar, = Currency.search([('code', '=', 'USD')])
        party = Party(Transaction().context['active_id'])
        pyson_domain = [
                ('move.company', '=', Transaction().context['company']),
                ('party', '=', Transaction().context['active_id']),
                ['OR',
                    ('account.type.payable', '=', True),
                    ('account.type.receivable', '=', True)],
                ['second_currency', '=', id_dollar.id]
                ]
        pyson_context = {
                'company': Transaction().context['company'],
                'party': Transaction().context['active_id'],
                'account_kind': ['payable', 'receivable'],
                }

        if Transaction().context.get('from_date'):
            pyson_domain.append(
                ('date', '>=', Transaction().context['from_date']))
            pyson_context['from_date'] = Transaction().context['from_date']
        if Transaction().context.get('to_date'):
            pyson_domain.append(
                ('date', '<=', Transaction().context['to_date']))
            pyson_context['to_date'] = Transaction().context['to_date']

        action['pyson_domain'] = PYSONEncoder().encode(pyson_domain)
        action['pyson_context'] = PYSONEncoder().encode(pyson_context)
        action['name'] = 'Cuenta corriente dólares - %s' % (party.name)
        return action, {}


class StatementOfAccountDollarReport(CompanyReport):
    'Statement of Account Dollars'
    __name__ = 'account.move.line.move_line_list_dollar'


class StatementOfAccountDollarSpreadsheet(CompanyReport):
    'Statement of Account Dollars (Spreadsheet)'
    __name__ = 'account.move.line.move_line_list_dollar_spreadsheet'
