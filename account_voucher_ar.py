# This file is part of the flexar_silix module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from decimal import Decimal
from collections import defaultdict, namedtuple
from itertools import combinations

from trytond.model import fields, ModelView, Workflow
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction
from trytond.exceptions import UserError
from trytond.i18n import gettext

_ZERO = Decimal('0.0')


class AccountVoucher(metaclass=PoolMeta):
    __name__ = 'account.voucher'

    @fields.depends('company', 'currency', 'writeoff')
    def on_change_currency(self):
        pool = Pool()
        Configuration = pool.get('account.configuration')

        super().on_change_currency()

        if (not self.writeoff and self.currency and
                self.currency != self.company.currency):
            config = Configuration(1)
            writeoff = config.get_multivalue(
                'default_voucher_writeoff_dollar')
            if writeoff:
                self.writeoff = writeoff

    def add_lines(self):
        # Replaces original add_lines() method
        # to manage imported balance from different system
        pool = Pool()
        Invoice = pool.get('account.invoice')
        MoveLine = pool.get('account.move.line')
        InvoiceAccountMoveLine = pool.get('account.invoice-account.move.line')
        Currency = pool.get('currency.currency')
        AccountVoucherLineCredits = pool.get('account.voucher.line.credits')
        AccountVoucherLineDebits = pool.get('account.voucher.line.debits')
        AccountVoucherLine = pool.get('account.voucher.line')

        lines = []
        lines_credits = []
        lines_debits = []

        if not self.currency or not self.party:
            self.lines = lines
            self.lines_credits = lines_credits
            self.lines_debits = lines_debits
            return

        if self.lines:
            return

        second_currency = None
        if self.currency != self.company.currency:
            second_currency = self.currency

        clause = [
            ('party', '=', self.party),
            ('state', '=', 'valid'),
            ('reconciliation', '=', None),
            ('move.state', '=', 'posted'),
            ]
        if self.voucher_type == 'receipt':
            clause.append(('account.type.receivable', '=', True))
        else:
            clause.append(('account.type.payable', '=', True))

        if self.pay_invoice:
            move_lines = self.pay_invoice.lines_to_pay
        else:
            move_lines = MoveLine.search(clause)

        for line in move_lines:
            origin = str(line.move_origin)
            origin = origin[:origin.find(',')]
            # Do not filter by origin
            # if origin not in [
                    # 'account.invoice',
                    # 'account.voucher',
                    # 'account.statement']:
                # continue

            invoice = InvoiceAccountMoveLine.search([
                ('line', '=', line.id),
                ])
            if invoice:
                continue

            if line.credit:
                line_type = 'cr'
                amount = line.credit
            else:
                amount = line.debit
                line_type = 'dr'

            amount_residual = abs(line.amount_residual)
            currency_rate = None
            if second_currency:
                if line.second_currency == self.currency:
                    currency_rate = Decimal(
                        amount / abs(line.amount_second_currency)).quantize(
                        Decimal(str(10 ** -6)))
                with Transaction().set_context(
                        currency_rate=currency_rate, date=self.date):
                    amount = Currency.compute(
                        self.company.currency, amount,
                        self.currency)
                    amount_residual = abs(line.amount_residual_second_currency)
                    #amount_residual = Currency.compute(
                        #self.company.currency, amount_residual,
                        #self.currency)

            name = ''
            model = str(line.move_origin)
            invoice_date = None
            if model[:model.find(',')] == 'account.invoice':
                invoice = Invoice(line.move_origin.id)
                invoice_date = invoice.invoice_date
                if invoice.type[0:3] == 'out':
                    name = invoice.number
                else:
                    name = invoice.reference
            else:
                if line.description:
                    name = line.description
                else:
                    if line.move_origin is None:
                        # Move with no origin: get description
                        name = line.move.description

            if line.credit and self.voucher_type == 'receipt':
                payment_line = AccountVoucherLineCredits()
            elif line.debit and self.voucher_type == 'payment':
                payment_line = AccountVoucherLineDebits()
            else:
                payment_line = AccountVoucherLine()
            payment_line.name = name
            payment_line.account = line.account.id
            payment_line.amount = _ZERO
            payment_line.amount_original = amount
            payment_line.amount_unreconciled = amount_residual
            payment_line.line_type = line_type
            payment_line.move_line = line.id
            payment_line.date = invoice_date or line.date
            payment_line.currency_rate = currency_rate

            if line.credit and self.voucher_type == 'receipt':
                lines_credits.append(payment_line)
            elif line.debit and self.voucher_type == 'payment':
                lines_debits.append(payment_line)
            else:
                if (payment_line.move_line and
                        payment_line.move_line.amount_second_currency):
                    payment_line.amount_second_currency = abs(
                        payment_line.move_line.amount_second_currency)
                lines.append(payment_line)

        self.lines = sorted(lines, key=lambda x: x.date)
        self.lines_credits = lines_credits
        self.lines_debits = lines_debits

    def create_move(self, move_lines):
        # Replaces original create_move() method
        # to reconcile lines not linked to invoices
        pool = Pool()
        Move = pool.get('account.move')
        MoveLine = pool.get('account.move.line')
        Invoice = pool.get('account.invoice')
        Currency = pool.get('currency.currency')

        created_lines = MoveLine.create(move_lines)
        Move.post([self.move])

        lines_to_reconcile = defaultdict(list)
        payment_lines_to_relate = defaultdict(list)

        for line in self.lines:
            origin = str(line.move_line.move_origin)
            origin = origin[:origin.find(',')]
            # Do not filter by origin
            # if origin not in ['account.invoice',
                    # 'account.voucher']:
                # continue
            if line.amount == _ZERO:
                continue

            amount_second_currency = (line.amount if
                self.currency != self.company.currency else None)
            if origin in ['account.invoice', 'account.voucher']:
                invoice = Invoice(line.move_line.move_origin.id)
                with Transaction().set_context(
                        currency_rate=self.currency_rate, date=self.date):
                    amount = Currency.compute(self.currency,
                        line.amount, self.company.currency)

                reconcile_lines, remainder = \
                    self.get_reconcile_lines_for_amount(invoice, amount,
                        amount_second_currency,
                        lines_to_reconcile[line.account.id])

                if remainder == _ZERO:
                    for reconcile_line in reconcile_lines:
                        lines_to_reconcile[line.account.id].append(
                            reconcile_line.id)

                for move_line in created_lines:
                    if move_line.party is None:
                        continue
                    if move_line.description == 'advance':
                        continue
                    if (move_line.debit != abs(amount) and
                            move_line.credit != abs(amount) and
                            (move_line.amount_second_currency and
                            abs(move_line.amount_second_currency) !=
                            abs(line.amount))):
                        continue
                    invoice_number = invoice.reference
                    if invoice.type == 'out':
                        invoice_number = invoice.number
                    if move_line.description == invoice_number:
                        if remainder == _ZERO:
                            lines_to_reconcile[move_line.account.id].append(
                                move_line.id)
                        payment_lines_to_relate[invoice].append(move_line.id)
            else:
                with Transaction().set_context(
                        currency_rate=self.currency_rate, date=self.date):
                    amount = Currency.compute(self.currency,
                        line.amount, self.company.currency)

                reconcile_lines, remainder = \
                    self.get_reconcile_move_lines_for_amount(
                        line.move_line.move, amount,
                        amount_second_currency,
                        lines_to_reconcile[line.account.id])

                if remainder == _ZERO:
                    for reconcile_line in reconcile_lines:
                        lines_to_reconcile[line.account.id].append(
                            reconcile_line.id)

                for move_line in created_lines:
                    if move_line.party is None:
                        continue
                    if move_line.description == 'advance':
                        continue
                    if (move_line.debit != abs(amount) and
                            move_line.credit != abs(amount) and
                            (move_line.amount_second_currency and
                            abs(move_line.amount_second_currency) !=
                            abs(line.amount))):
                        continue
                    if remainder == _ZERO:
                        lines_to_reconcile[move_line.account.id].append(
                            move_line.id)

        if payment_lines_to_relate:
            for invoice, payment_lines in payment_lines_to_relate.items():
                Invoice.write([invoice], {
                    'payment_lines': [('add', list(set(payment_lines)))],
                    })

        if lines_to_reconcile:
            for lines_ids in lines_to_reconcile.values():
                if not lines_ids:
                    continue
                lines = MoveLine.browse(list(set(lines_ids)))
                MoveLine.reconcile(lines)

        reconcile_lines = []
        if self.lines_credits:
            for line in self.lines_credits:
                reconcile_lines.append(line.move_line)
            for move_line in created_lines:
                if move_line.description == 'advance':
                    reconcile_lines.append(move_line)
        if reconcile_lines:
            MoveLine.reconcile(reconcile_lines)

        reconcile_lines = []
        if self.lines_debits:
            for line in self.lines_debits:
                reconcile_lines.append(line.move_line)
            for move_line in created_lines:
                if move_line.description == 'advance':
                    reconcile_lines.append(move_line)
        if reconcile_lines:
            MoveLine.reconcile(reconcile_lines)

        return True

    def get_reconcile_move_lines_for_amount(
            self, move, amount, amount_second_currency, reconcile_lines):
        '''
        Return list of lines and the remainder to make reconciliation.
        This is an alternative from 'get_reconcile_lines_for_amount' method
        '''
        if self.voucher_type == 'payment':
            amount = -amount
            if amount_second_currency:
                amount_second_currency = -amount_second_currency
        party = self.party
        Result = namedtuple('Result', ['lines', 'remainder'])

        lines = [
            l for l in move.lines
            if not l.reconciliation
            and (l.account.party_required or l.party == party)
            and (l.id not in reconcile_lines)]

        total_amount = 0
        for l in lines:
            total_amount += l.debit - l.credit
        best = Result([], total_amount)
        for n in range(len(lines), 0, -1):
            for comb_lines in combinations(lines, n):
                remainder = sum((l.debit - l.credit)
                    for l in comb_lines)
                remainder -= amount
                result = Result(list(comb_lines), remainder)
                if remainder == _ZERO:
                    return result
                if abs(remainder) < abs(best.remainder):
                    best = result
        if amount_second_currency:
            best = Result([], total_amount)
            for n in range(len(lines), 0, -1):
                for comb_lines in combinations(lines, n):
                    remainder = sum((l.amount_second_currency)
                        for l in comb_lines)
                    remainder -= amount_second_currency
                    result = Result(list(comb_lines), remainder)
                    if remainder == _ZERO:
                        return result
                    if abs(remainder) < abs(best.remainder):
                        best = result
        return best

    @classmethod
    def check_lines_wo_origin(cls, vouchers):
        '''
        Check lines without origin, that is not linked to an invoice
        '''
        for voucher in vouchers:
            lines = [l for l in voucher.lines
                if l.move_line.move_origin is None]
            for l in lines:
                if l.amount_unreconciled != l.amount:
                    raise UserError(gettext(
                        'flexar_silix.msg_non_origin_lines_full_payment'))

    @classmethod
    @ModelView.button
    @Workflow.transition('posted')
    def post(cls, vouchers):
        cls.check_lines_wo_origin(vouchers)
        super().post(vouchers)


class AccountVoucherLine(metaclass=PoolMeta):
    __name__ = 'account.voucher.line'

    amount_second_currency = fields.Numeric(
        'Original Amount (Second Currency)', digits=(16, 2),
        states={'readonly': True})
