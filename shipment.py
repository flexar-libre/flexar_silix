# This file is part of the flexar_silix module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import stdnum.ar.cuit as cuit

from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval


class ShipmentOut(metaclass=PoolMeta):
    __name__ = 'stock.shipment.out'

    comment = fields.Text('Comment',
        states={'readonly': Eval('state').in_(['cancelled', 'done'])})

    @classmethod
    def create(cls, vlist):
        vlist = [x.copy() for x in vlist]
        for values in vlist:
            if values.get('number') is None:
                values['number'] = ' '
        shipments = super().create(vlist)
        return shipments

    @classmethod
    def done(cls, shipments):
        pool = Pool()
        Config = pool.get('stock.configuration')

        config = Config(1)
        super().done(shipments)
        for shipment in shipments:
            if shipment.number is None or shipment.number in (' ', ''):
                shipment.number = config.get_multivalue(
                    'shipment_out_sequence',
                    company=shipment.company.id).get()
                shipment.save()


class DeliveryNote(metaclass=PoolMeta):
    __name__ = 'stock.shipment.out.delivery_note'

    @classmethod
    def get_context(cls, shipments, header, data):
        report_context = super().get_context(shipments, header, data)

        # Only one record at a time
        report_context['records'] = [report_context['record']]
        report_context['data']['ids'] = [report_context['data']['id']]
        shipment = report_context['record']
        report_context['customer_vat_number'] = cls._get_customer_vat_number(
            shipment)
        report_context['comment'] = shipment.comment and shipment.comment \
            or ''
        report_context['report_moves'] = cls._get_report_moves

        return report_context

    @classmethod
    def _get_customer_vat_number(cls, shipment):
        value = ''
        if shipment.customer.vat_number:
            value = shipment.customer.vat_number
            return cuit.format(value)
        return value

    @classmethod
    def _get_report_moves(cls, shipment):
        moves = []
        for outgoing_move in shipment.outgoing_moves:
            if outgoing_move.product not in ([m['product'] for m in moves] if moves else []):
                move = {
                    'product': outgoing_move.product,
                    'quantity': outgoing_move.quantity,
                    'uom': outgoing_move.uom,
                    'product_customer': outgoing_move.product_customer if
                    outgoing_move.product_customer else None
                }
                moves.append(move)
            else:
                for move in moves:
                    if move['product'] == outgoing_move.product:
                        move['quantity'] += outgoing_move.quantity
        return moves
